import Component from '@ember/component';

export default Component.extend({

	collapse: true,

	actions: {
		collapse() {
			this.toggleProperty('collapsed');
		}
	}

});
